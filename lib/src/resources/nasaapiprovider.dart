import 'dart:convert';

import 'package:http/http.dart';

import '../models/nasaapodmodel.dart';
import '../models/nasaneomodel.dart';

class NasaApiProvider {
  final String _apiKey = 'https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY';
  final String _api = 'https://api.nasa.gov/neo/rest/v1/feed?start_date=2019-09-07&end_date=2019-09-13&api_key=DEMO_KEY';

  Future<NasaApodModel> fetchNasaApodData() async {
    var data = await get(_apiKey);

    if(data.statusCode == 200) {
      return NasaApodModel.fromJson(jsonDecode(data.body));
    }
    else {
      throw Exception('Could not retrieve data');
    }
  }

  Future<NasaNeoModel> fetchNasaNeoData() async {
    var data = await get(_api);

    if(data.statusCode == 200) {
      return NasaNeoModel.fromJson(json.decode(data.body));
    }
    else {
      throw Exception('Could not retrieve data');
    }
  }
}