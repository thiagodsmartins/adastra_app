import 'package:adastra_app/src/models/nasaapodmodel.dart';
import 'package:adastra_app/src/models/nasaneomodel.dart';

import 'nasaapiprovider.dart';

class Repository {
  final NasaApiProvider _nasaApiProvider = NasaApiProvider();

  Future<NasaApodModel> fetchNasaApodData() => _nasaApiProvider.fetchNasaApodData();
  Future<NasaNeoModel>  fetchNasaNeoData()  => _nasaApiProvider.fetchNasaNeoData();
}