import 'package:adastra_app/src/blocs/nasaneobloc.dart';
import 'package:adastra_app/src/models/nasaneomodel.dart';
import 'package:flutter/material.dart';

class NasaNeoUi extends StatefulWidget {
  @override
  _NasaNeoUiState createState() => _NasaNeoUiState();
}

class _NasaNeoUiState extends State<NasaNeoUi> {
  List<String> list = ['valor1', 'valor2', 'valor3', 'valor4'];
  @override
  void initState() {
    super.initState();
    blocNasaNeo.fetchNeo();
  }

  @override
  void dispose() {
    super.dispose();
    blocNasaNeo.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Nasa Near Earth Objects'),
      ),
      body: Center(
        child: StreamBuilder(
          stream: blocNasaNeo.request,
          builder: (context, AsyncSnapshot<NasaNeoModel> snapshot) {
            if(snapshot.hasData) {
              return ListView.builder(
                itemCount: snapshot.data.nasaNeoRequest.length,
                itemBuilder: (BuildContext context, index) {
                  return ExpansionTile(
                    title: Text(snapshot.data.nasaNeoRequest[index]['name']),
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text('ID:' + snapshot.data.nasaNeoRequest[index]['id']),
                        ],
                      )
                    ],
                  );
                }
              );
            }
            else if(snapshot.hasError) {
              return Text('Error');
            }
            return CircularProgressIndicator();
          },
        ),
      ),
    );
  }
}
