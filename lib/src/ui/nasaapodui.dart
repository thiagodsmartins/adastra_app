import 'package:adastra_app/src/blocs/nasaapodbloc.dart';
import 'package:adastra_app/src/models/nasaapodmodel.dart';
import 'package:flutter/material.dart';

class NasaApodUi extends StatefulWidget {
  @override
  _NasaApodUiState createState() => _NasaApodUiState();
}

class _NasaApodUiState extends State<NasaApodUi> {
  @override
  void initState() {
    super.initState();
    blocNasaApod.fetchApod();
  }

  @override
  void dispose() {
    super.dispose();
    blocNasaApod.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text('Nasa Apod')
        ),
        body: Center(
          child: StreamBuilder(
            stream: blocNasaApod.request,
            builder: (context, AsyncSnapshot<NasaApodModel> snapshot) {
              if(snapshot.hasData) {
                return Image.network(snapshot.data.nasaApodRequest.image);
              }
              else if(snapshot.hasError){
                return Text('Error');
              }
              return CircularProgressIndicator();
            },
          ),
        )
    );
  }}

