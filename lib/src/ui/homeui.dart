import 'dart:ui';

import 'package:adastra_app/src/ui/NasaApodUi.dart';
import 'package:adastra_app/src/ui/nasaneoui.dart';
import 'package:flutter/material.dart';

class HomeUi extends StatelessWidget {
  final String title;

  HomeUi({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Ad Astra Vers:1.0'),
        backgroundColor: Colors.blue,
      ),
      body: Center(child: Text('Ad Astra App')),
      drawer: Drawer(
        child: BackdropFilter(
          filter: ImageFilter.blur(
            sigmaX: 6.0,
            sigmaY: 6.0,
          ),
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              DrawerHeader(
                child: Text('Ad Astra'),
                decoration: BoxDecoration(
                  color: Colors.blue,
                ),
              ),
              ListTile(
                title: Text('Image of the Day'),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.push(context, MaterialPageRoute(builder: (context) => NasaApodUi()));
                },
              ),
              ListTile(
                title: Text('Asteroids Near Earth'),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.push(context, MaterialPageRoute(builder: (context) => NasaNeoUi()));
                },
              ),
            ],
          ),
        )
      ),
    );
  }
}