import 'package:adastra_app/src/models/nasaneomodel.dart';
import 'package:adastra_app/src/resources/repository.dart';
import 'package:rxdart/rxdart.dart';

class NasaNeoBloc {
  PublishSubject<NasaNeoModel> _publishSubject = PublishSubject<NasaNeoModel>();
  Repository _repository = Repository();

  PublishSubject<NasaNeoModel> get request => _publishSubject.stream;

  void fetchNeo() async {
    NasaNeoModel nnm = await _repository.fetchNasaNeoData();
    _publishSubject.sink.add(nnm);
  }

  void dispose() async {
    await _publishSubject.drain();
    _publishSubject.close();
  }
}

final NasaNeoBloc blocNasaNeo = NasaNeoBloc();