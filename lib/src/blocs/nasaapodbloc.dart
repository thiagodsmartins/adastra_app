import 'package:adastra_app/src/models/nasaapodmodel.dart';
import 'package:adastra_app/src/resources/repository.dart';
import 'package:rxdart/rxdart.dart';

class NasaApodBloc {
  PublishSubject<NasaApodModel> _publishSubject = PublishSubject<NasaApodModel>();
  Repository _repository = Repository();
  bool state = false;

  PublishSubject<NasaApodModel> get request => _publishSubject.stream;

  void fetchApod() async {
    NasaApodModel nam = await _repository.fetchNasaApodData();
    _publishSubject.sink.add(nam);
  }

  void dispose() async {
    await _publishSubject.drain();
    _publishSubject.close();
  }
}

final NasaApodBloc blocNasaApod = NasaApodBloc();