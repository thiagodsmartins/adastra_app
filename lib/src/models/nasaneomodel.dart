class NasaNeoModel {
  List<dynamic> _nasaNeoRequest;
  int _elementCount;

  NasaNeoModel.fromJson(Map<String, dynamic> jsonData) {
    _elementCount = jsonData['element_count'];
    _nasaNeoRequest = jsonData['near_earth_objects']['2019-09-09'];
    _nasaNeoRequest += jsonData['near_earth_objects']['2019-09-13'];

  }

  int get elementCount => _elementCount;
  List<dynamic> get nasaNeoRequest => _nasaNeoRequest;

}

class _NasaNeoRequest {
  String _name;

  String get name => _name;
  set name(value) => _name = value;
}