import 'package:flutter/material.dart';

class NasaApodModel {
  _NasaApodRequest _nasaApodRequest;

  NasaApodModel.fromJson(Map<String, dynamic> jsonData) {
    _nasaApodRequest = _NasaApodRequest();

    _nasaApodRequest.copyright = jsonData['copyright'];
    _nasaApodRequest.date = jsonData['date'];
    _nasaApodRequest.explanation = jsonData['explanation'];
    _nasaApodRequest.title = jsonData['title'];
    _nasaApodRequest.image = jsonData['hdurl'];
  }

  _NasaApodRequest get nasaApodRequest => _nasaApodRequest;

}

class _NasaApodRequest {
  String _copyRight;
  String _date;
  String _explanation;
  String _title;
  String _hdImage;

  String get copyright => _copyRight;
  String get date => _date;
  String get explanation => _explanation;
  String get title => _title;
  String get image => _hdImage;

  set copyright(value) => _copyRight = value;
  set date(value) => _date = value;
  set explanation(value) => _explanation = value;
  set title(value) => _title = value;
  set image(value) => _hdImage = value;

}